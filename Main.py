import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from ViewModels.GlavniProzor import GlavniProzor
from Assets.Sesiomat import kreirajSesiju

app = QApplication(sys.argv)

sesija = kreirajSesiju()

prozor = GlavniProzor(sesija)
prozor.show()

sys.exit(app.exec())


